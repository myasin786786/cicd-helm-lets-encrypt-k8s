# Multi stage Docker image build
FROM golang:1.18.2-alpine3.16 as build_base

RUN mkdir /build
COPY ./app /build

ARG GO_VER="1.4"
RUN sed -i "s/1.4/$GO_VER/g" /build/outyet/main.go
# RUN sed -i "/<\/center><\/body><\/html>/i<p>Built: $BUILT_ON<\/p>" /build/outyet/main.go

WORKDIR /build/outyet

RUN go build -o outyet .

FROM alpine:3.9
COPY --from=build_base /build/outyet/outyet /app/outyet

EXPOSE 8080

CMD ["/app/outyet"]

